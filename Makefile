CC		= x86_64-w64-mingw32-gcc
FLAGS	= -mwindows -mconsole
LIBS	= /cygdrive/c/windows/system32/winmm.dll
TARGET	= midi.exe
SRCS	= main.cpp

DEBUG_FLAGS		=
RELEASE_FLAGS	=

.PHONY: debug
debug: FLAGS+=$(DEBUG_FLAGS)
debug: all

.PHONY: release
release: FLAGS+=$(RELEASE_FLAGS)
release: all

all: $(TARGET)
$(TARGET): $(SRCS)
	$(CC) $(FLAGS) -o $(TARGET) $(SRCS) $(LIBS)

.PHONY: clean
clean:
	rm $(TARGET)

